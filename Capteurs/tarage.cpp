/**
 *  @file     tarage.cpp
 *  @license  BSD (see license.txt)
 *  @brief    programme pour effectuer le tarage de la balance
 *            ce programme détermine la constante offset et
 *            l'enregistre dans le fichier configuration.ini
 *            compilation: g++ tarage.cpp hx711.cpp SimpleIni.cpp  spi.c -o tarage
*/
#include <iostream>
#include <fstream>
#include <iomanip>
#include "hx711.h"
#include "SimpleIni.h"
// definition du chemin de pour le fichier de configuration.ini
#define CONFIGURATION "/home/pi/Ruche/configuration.ini"

using namespace std;
int main()
{

    hx711 balance;
    SimpleIni ini;
    int gain;
//si le chargement du fichier de configuration
    if(!ini.Load(CONFIGURATION))
    {
// message d'erreur si le chargement echoue
        cout << "Impossible d'ouvrir le fichier configuration.ini !!" << endl;
        return -1;
    }
// prend la valeur du gain dans le fichier de configuration et sinon prend la valeur 128 par défaut
    gain   = ini.GetValue<int>("balance", "gain", 128);
    balance.configurerGain(gain);

    /** début de la procédure de tarage
	10 mesures du poids sont effectuées
        Les plus grande et plus petite valeurs sont retirées
        puis la moyenne est calculée avec les 8 mesures restantes
    */
    int x1, max, min, som, offset;
    som = balance.obtenirValeur();
    cout << som << endl;
    min = som;
    max = som;
// pour 10 valeur prise on enleve la plus petite et la plus grand pour faire une moyen avec les 8 restant
    for(int i=1 ; i<10 ; i++)
    {
// utilisation de la methode obtenirValeur pour obtenir les valeurs
        x1 = balance.obtenirValeur();
        cout << x1 << endl;
        som += x1;
// selection de la plus grande
        if (x1 > max) max = x1;
// selection de la plus petite
        if (x1 < min) min = x1;
    }
// moyen des 8 valeur
    offset = (som - max - min)/ 8;
// affichage de l'offset
    cout << "offset : " << offset << endl;
// inscription de la valeur de l'offset dans le fichier de configuration
    ini.SetValue<int>("balance", "offset", offset);
// si le fichier ini n'a pas sauvegarder 
    if(!ini.Save())
    {
// message pour confirmer l'impossibiliter d'ecriture dans le ficher de configuration et retourne -1 
        cout << "Impossible d'écrire dans configuration.ini." << endl;
        return -1;
    }
// message de confiramtion quand l'offset a bien été enregistrer dans le fichier de configuration
    cout << "La valeur de l'offset a été enregistrée dans le fichier configuration.ini" << endl;

    return 0;

}
