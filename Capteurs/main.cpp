/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: aleborre
 *
 * Created on 30 avril 2019, 15:11
 */

#include <cstdlib>

#include <iostream>
#include <fstream>
#include <iomanip>

#include <cppconn/driver.h>
#include <cppconn/statement.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

#include "hx711.h"
#include "SimpleIni.h"

using namespace std;
using namespace sql;
/*
 * 
 */
int main() {


    sql::ConnectOptionsMap connexion_locale, connexion_distante;
    sql::Driver *driver;
    sql::Connection *con;
    sql::PreparedStatement *pstmt;
    SimpleIni ini;
    hx711 balance;

    // lecture du fichier de configuration.ini

    ini.Load("/home/pi/Ruche/configuration.ini");

    // attribution des information de connection a la bdd avec des valeurs par defaut

    connexion_distante["hostName"] = (sql::SQLString)ini.GetValue<string>("BDdistante", "host", "172.18.58.129");
    connexion_distante["userName"] = (sql::SQLString)ini.GetValue<string>("BDdistante", "user", "ruche");
    connexion_distante["password"] = (sql::SQLString)ini.GetValue<string>("BDdistante", "passwd", "touchard72");
    connexion_distante["schema"] = (sql::SQLString)ini.GetValue<string>("BDdistante", "bdd", "ruche");
    connexion_distante["port"] = 3306;
    connexion_distante["OPT_RECONNECT"] = false;

    connexion_locale["hostName"] = (sql::SQLString)ini.GetValue<string>("BDlocal", "host", "172.18.58.220");
    connexion_locale["userName"] = (sql::SQLString)ini.GetValue<string>("BDlocal", "user", "ruche");
    connexion_locale["password"] = (sql::SQLString)ini.GetValue<string>("BDlocal", "passwd", "touchard72");
    connexion_locale["schema"] = (sql::SQLString)ini.GetValue<string>("BDlocal", "bdd", "ruche");
    connexion_locale["port"] = 3306;
    connexion_locale["OPT_RECONNECT"] = false;

    // configuration de la balance avec valeur par defaut

    balance.fixerEchelle(ini.GetValue<float>("balance", "scale", 1.0));
    balance.fixerOffset(ini.GetValue<float>("balance", "offset", 0));
    balance.configurerGain(ini.GetValue<float>("balance", "gain", 128));

    // connection a la bdd distante
    try {
        driver = get_driver_instance();
        con = driver->connect(connexion_distante);

        // verification de la connection
        if (con->isValid()) {
            cout << "connexion établie avec le serveur Mysql distant" << endl;
        }
    }
    // connection a la bdd local
    catch (sql::SQLException &e) {
        try {
            driver = get_driver_instance();
            con = driver->connect(connexion_locale);
            // verification de la connection
            if (con->isValid()) {
                cout << "connexion établie avec le serveur Mysql local" << endl;
            }

        }        catch (sql::SQLException &e) {
            // Gestion des exceptions pour afficher les erreurs

            cout << "# ERR: SQLException in " << __FILE__;
            cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
            cout << "# ERR: " << e.what();
            cout << " (code erreur MySQL: " << e.getErrorCode();
            cout << ", EtatSQL: " << e.getSQLState() << " )" << endl;
            return 1;
        }
    }
    // traitement
    string sql("INSERT INTO feeds( id_channel , field1) VALUES (?,?)");
    pstmt = con->prepareStatement(sql);
    pstmt->setDouble(2, balance.obtenirPoids());
    pstmt->setInt(1, ini.GetValue<int>("ruche", "id", 0));
    pstmt->executeUpdate();

    delete pstmt;
    delete con;

    cout << endl;
    return EXIT_SUCCESS;
}


