/**
 *  @file     etalonnage.cpp
 *  @license  BSD (see license.txt)
 *  @brief    programme étalonnage de la balance
 *            ce programme détermine les constantes offset scale et gain et
 *            les enregistre dans le fichier configuration.ini
 *            compilation: g++ etalonnage.cpp hx711.cpp SimpleIni.cpp  spi.c -o etalonnage
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include "hx711.h"
#include "SimpleIni.h"
// definition du chemin du fichier de configuration
#define CONFIGURATION "/home/pi/Ruche/configuration.ini"

using namespace std;

int main()
{
// instancie une balance
    hx711 balance;
// instancie ini
    SimpleIni ini;

    int x1,x2;
    int offset;
    float scale;
    int gain;
    float poids;
    string unite;
    int precision;
// si le fichier ne veut pas charger
    if(!ini.Load(CONFIGURATION))
    {
// message de confirmant l'erreur et retourne une valeur -1
        cout << "Impossible d'ouvrir le fichier configuration.ini !!" << endl;
        return -1;
    }
    // demande de l'unite de mesure en gramme , kg ou lb
    cout << "Quelle est l'unité de mesure ? (kg lb)" << endl;
// inscrit l'unite choisie dans le fichier de configuration
    cin >> unite;
    ini.SetValue<string>("balance", "unite", unite);
// fait une demande pour avoir la precision d'affichage avec 1 ou 2 chiffre avec une boucle infinie de demande si la reponse n'est pas 1 ou 2
    do
    {
        cout << "Quelle est la précision d'affichage : 1 ou 2 chiffres après la virgule" << endl;
        cin >> precision;
    }
    while (precision !=1 && precision !=2);
    ini.SetValue<int>("balance", "precision", precision);
// pareille que la precision mais avec le gain
    do
    {
        cout << "Donnez le gain souhaité : 128 ou 64 ? " << endl;
        cin >> gain;
    }
    while (gain !=128 && gain !=64);
// on inscrit le gain dans le fichier de configuration
    ini.SetValue<int>("balance", "gain", gain);

    balance.configurerGain(gain);
    int max,min,som;
    som = balance.obtenirValeur();
    min = som;
    max = som;
// on recupere 100 valeur , avec la fonction obtenirValeur
    for(int i=1 ; i<100 ; i++)
    {
        x1 = balance.obtenirValeur();
        cout << x1 << endl;
        som += x1;
        if (x1 > max) max = x1;
        if (x1 < min) min = x1;
    }
// moyenne des valeurs obtenu mais sans la plus grande et plus petite valeur
    offset = (som - max - min)/ 98;
    cout << "offset : " << offset << endl;
// on inscrit l'offset obtenu dans le fichier de configuration
    ini.SetValue<int>("balance", "offset", offset);
// demande du poids poser sur la balance pour l'etalonnage
    cout << "Posez un poids étalon sur le plateau et donnez sa valeur en " << unite << " : " << endl;
    cin >> poids;
    cout << "Vous avez posé un poids de " << poids << " " << unite << endl;
// obtention des valeurs avec la fonction obtenirValeur
    som = balance.obtenirValeur();
    max = som;
    min = som;
// boucle "pour" qui prend 100 valeur et supprime la plus grande et la plus petite valeurs pour faire ensuite une moyenne 
    for(int i=1 ; i<100 ; i++)
    {
        x2 = balance.obtenirValeur();
        cout << x2 << endl;
        som += x2;
        if (x2 > max) max = x2;
        if (x2 < min) min = x2;

    }
// moyenne de x2
    x2 = (som - max - min)/98 ;
// calcule du scale 
    scale = (float)(x2 - offset)/poids;
    cout << "scale : " << scale  << endl;
// on inscrit le scale dans le fichier de configuration
    ini.SetValue<float>("balance", "scale", scale);

// si la sauvegarde du fichier de configuration n'a pas fonctionner
    if(!ini.SaveAs(CONFIGURATION))
    {
// message d'erreur et retourne -1
        cout << "Impossible d'écrire dans configuration.ini." << endl;
        return -1;
    }
// message de confirmation du bon deroulement de la sauvegarde dans le fichie de configuration
    cout << "Le fichier de configuration : configuration.ini a été enregistré." << endl;

    return 0;

}


